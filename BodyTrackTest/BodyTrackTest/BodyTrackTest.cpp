﻿// BodyTrackTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <chrono>
	
#include <k4a/k4a.h>
#include <k4arecord/record.h>
#include <k4arecord/playback.h>

#define cpp_test

#ifdef cpp_test
#include <k4abt.hpp>
#else
#include <k4abt.h>
#endif

#define CAPTURE_TIMEOUT_MS 100

#ifndef cpp_test
int main()
{
	k4a_device_t device = NULL;
	k4a_device_open(0, &device);

	// Start camera. Make sure depth camera is enabled.
	k4a_device_configuration_t deviceConfig = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
	deviceConfig.depth_mode = K4A_DEPTH_MODE_NFOV_UNBINNED;
	deviceConfig.color_resolution = K4A_COLOR_RESOLUTION_OFF;
	k4a_device_start_cameras(device, &deviceConfig);

	k4a_calibration_t sensor_calibration;
	k4a_device_get_calibration(device, deviceConfig.depth_mode, deviceConfig.color_resolution, &sensor_calibration);

	k4abt_tracker_t tracker = NULL;
	k4abt_tracker_configuration_t tracker_config = K4ABT_TRACKER_CONFIG_DEFAULT;
	k4abt_tracker_create(&sensor_calibration, tracker_config, &tracker);


	while (1) {
		auto start = std::chrono::high_resolution_clock::now();

		k4a_capture_t capture;

		k4a_device_get_capture(device, &capture, CAPTURE_TIMEOUT_MS);


		k4a_wait_result_t queue_capture_result = k4abt_tracker_enqueue_capture(tracker, capture, K4A_WAIT_INFINITE);
		k4a_capture_release(capture);
		if (queue_capture_result != K4A_WAIT_RESULT_SUCCEEDED)
		{
			// It should never hit timeout or error when K4A_WAIT_INFINITE is set.
			printf("Error! Adding capture to tracker process queue failed!\n");
			break;
		}

		k4abt_frame_t body_frame = NULL;
		k4a_wait_result_t pop_frame_result = k4abt_tracker_pop_result(tracker, &body_frame, K4A_WAIT_INFINITE);
		if (pop_frame_result != K4A_WAIT_RESULT_SUCCEEDED)
		{
			// It should never hit timeout or error when K4A_WAIT_INFINITE is set.
			printf("Error! Popping body tracking result failed!\n");
			break;
		}

		size_t body_count = k4abt_frame_get_num_bodies(body_frame);

		for (size_t i = 0; i < body_count; i++)
		{
			k4abt_skeleton_t skeleton;
			k4abt_frame_get_body_skeleton(body_frame, i, &skeleton);
			uint32_t id = k4abt_frame_get_body_id(body_frame, i);

			std::cout << skeleton.joints[0].position.xyz.x << std::endl;
		}

		k4abt_frame_release(body_frame);

		auto finish = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> elapsed = finish - start;

		std::cout << elapsed.count() << " Body count: " << body_count << std::endl;

	}
}
#endif

#ifdef cpp_test
int main() {
	k4a::device NativeKinectDevice;
	k4abt::tracker NativeBodyTracker;
	try
	{
		// Open the Azure Kinect Device
		NativeKinectDevice = k4a::device::open(0);

		// Start the Camera and make sure the Depth Camera is Enabled
		k4a_device_configuration_t deviceConfig = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
		deviceConfig.depth_mode = K4A_DEPTH_MODE_NFOV_UNBINNED;
		deviceConfig.color_resolution = K4A_COLOR_RESOLUTION_OFF;

		NativeKinectDevice.start_cameras(&deviceConfig);

		// Get the device calibration
		k4a::calibration sensorCalibration = NativeKinectDevice.get_calibration(deviceConfig.depth_mode, deviceConfig.color_resolution);

		// Create the Body tracker using the calibration
		NativeBodyTracker = k4abt::tracker::create(sensorCalibration);
	}
	catch (k4a::error initError)
	{
		printf(":( %s\n", initError.what());
		return -1;
	}

	while (1) {

		auto start = std::chrono::high_resolution_clock::now();

		k4a::capture sensorCapture = nullptr;
		if (!NativeKinectDevice.get_capture(&sensorCapture))
		{
			printf(":/\n");
			return -1;
		}


		// Enqueue the capture
		if (!NativeBodyTracker.enqueue_capture(sensorCapture))
		{
			printf(":|\n");
			return -2;
		}


		k4abt::frame bodyFrame = nullptr;
		if (!NativeBodyTracker.pop_result(&bodyFrame))
		{
			printf("(╯°□°)╯︵ ┻━┻\n");
			return -3;
		}


		auto finish = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> elapsed = finish - start;

		printf("%f %d\n", elapsed.count(), bodyFrame.get_num_bodies());
	}


	return 0;
}
#endif // cpp_test